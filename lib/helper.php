<?php

use Bitrix\Main\Loader;
use Bitrix\Main\UserTable;

class ShaklHelper
{
    /**
     * Форматирует номер телефона, удаляя все символы, кроме цифр, и заменяя первую 8 на 7.
     *
     * @param string $phone Исходный номер телефона.
     * @return string Форматированный номер телефона.
     */
    protected static function formatPhoneNumber($phone): string
    {

        $formattedPhone = preg_replace('/[^\d]/', '', $phone);
        if (strlen($formattedPhone) == 11 && $formattedPhone[0] == '8') {
            $formattedPhone[0] = '7';
        }
        return $formattedPhone;
    }

    /**
     * Возвращает идентификатор пользователя по номеру телефона.
     *
     * @param string $phone Номер телефона пользователя.
     * @return int|false Идентификатор пользователя или false, если пользователь не найден.
     */
    public static function getUserIdByPhone($phone)
    {
        $formattedPhone = self::formatPhoneNumber($phone);

        if (!Loader::includeModule('main')) {
            throw new \Exception("Не удалось подключить модуль main");
        }

        $filter = ['=UF_BXMAKER_AUPHONE' => $formattedPhone];
        $select = ['ID'];

        $res = UserTable::getList([
            'select' => $select,
            'filter' => $filter
        ]);

        if ($row = $res->fetch()) {
            return $row["ID"];
        }

        return false;
    }

    /**
     * Копирует базовую цену товаров в новый тип цены для каждого города, указанного в названии инфоблока.
     * Исключает инфоблоки торговых предложений.
     *
     * @return void
     */
    public static function copyBasePriceToCityPrice()
    {
        global $APPLICATION;

        if (!Loader::includeModule("iblock") || !Loader::includeModule("catalog")) {
            echo "Не удалось подключить модули.\n";
            return;
        }

        $resCatalogs = CCatalog::GetList([], ['!PRODUCT_IBLOCK_ID' => 0], false, false, ['IBLOCK_ID']);
        $catalogs = [];
        while ($catalog = $resCatalogs->Fetch()) {
            $catalogs[$catalog['IBLOCK_ID']] = true;
        }

        $iblocks = CIBlock::GetList([], ['TYPE' => '1c_catalog'], true);
        while ($iblock = $iblocks->Fetch()) {
            if (array_key_exists($iblock['ID'], $catalogs)) {
                continue;
            }

            if (preg_match('/\((.*?)\)/', $iblock['NAME'], $matches)) {
                $cityName = $matches[1];

                $dbPriceType = CCatalogGroup::GetList([], ["NAME" => $cityName]);
                if (!$priceType = $dbPriceType->Fetch()) {
                    $arFields = [
                        "NAME" => $cityName,
                        "BASE" => "N",
                        "SORT" => 100,
                        "USER_LANG" => [
                            "ru" => $cityName,
                            "en" => $cityName,
                        ],
                        "USER_GROUP" => [2],
                        "USER_GROUP_BUY" => [2],
                    ];
                    $priceTypeId = CCatalogGroup::Add($arFields);
                    if (!$priceTypeId) {
                        $e = $APPLICATION->GetException();
                        if ($e) {
                            echo "Ошибка создания типа цены для города: $cityName. Текст ошибки: " . $e->GetString() . "\n";
                        } else {
                            echo "Ошибка создания типа цены для города: $cityName. Точная причина неизвестна.\n";
                        }
                        continue;
                    }
                } else {
                    $priceTypeId = $priceType['ID'];
                }

                $products = CIBlockElement::GetList([], ["IBLOCK_ID" => $iblock['ID']], false, false, ["ID"]);
                while ($product = $products->Fetch()) {
                    $productID = $product['ID'];
                    $res = CPrice::GetList([], ["PRODUCT_ID" => $productID, "CATALOG_GROUP_ID" => $priceTypeId]);
                    if ($arr = $res->Fetch()) {
                        CPrice::Update($arr["ID"], [
                            "PRICE" => $arr["PRICE"],
                            "CURRENCY" => $arr["CURRENCY"]
                        ]);
                    } else {
                        $basePrice = CPrice::GetBasePrice($productID);
                        CPrice::Add([
                            "PRODUCT_ID" => $productID,
                            "CATALOG_GROUP_ID" => $priceTypeId,
                            "PRICE" => $basePrice["PRICE"],
                            "CURRENCY" => $basePrice["CURRENCY"]
                        ]);
                    }
                }
            }
        }
    }
}